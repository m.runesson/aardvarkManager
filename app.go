package main

import (
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/app"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/config"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/internal/gitlab"
)

func main() {
	config := config.GetConfig()
	gitlabService := gitlab.NewGitLabService(config.GitUserName, config.GitUserEmail, config.GitlabToken)
	app.Launch(config, gitlabService)
}
