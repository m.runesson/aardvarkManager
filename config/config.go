package config

import (
	"fmt"
	viper "github.com/spf13/viper"
	"log"
	"strings"
)

const AdminGroup = "admins"

type Config struct {
	GitlabToken            string
	AardvarkWebhookUrl     string
	AardvarkWebhookSecret  string
	GitUserName            string
	GitUserEmail           string
	NamespaceConfigProject string
	ArgocdConfigProject    string
	ArgocdConfigNamespace  string
	LogRequests            bool
}

type Environment struct {
	Description string
	Cluster     string
}

const gitlabTokenKey = "gitlab.token"
const aardvarkWebhookUrlKey = "aardvark.webhook.url"
const aardvarkWebhookSecretKey = "aardvark.webhook.secret"
const gitUserEmailKey = "git.user_email"
const namespacesConfigProject = "namespaces.project"
const argocdConfigProject = "argocd.project"
const argocdConfigNamespace = "argocd.namespace"
const logRequests = "log.requests"

func GetConfig() Config {
	setDefaultValues()
	bindEnvironmentVariables()
	readConfigFile()
	cfg := Config{
		GitlabToken:            viper.GetString(gitlabTokenKey),
		AardvarkWebhookUrl:     viper.GetString(aardvarkWebhookUrlKey),
		AardvarkWebhookSecret:  viper.GetString(aardvarkWebhookSecretKey),
		GitUserName:            "Aardvark Manager",
		GitUserEmail:           viper.GetString(gitUserEmailKey),
		NamespaceConfigProject: viper.GetString(namespacesConfigProject),
		ArgocdConfigProject:    viper.GetString(argocdConfigProject),
		ArgocdConfigNamespace:  viper.GetString(argocdConfigNamespace),
		LogRequests:            viper.GetBool(logRequests),
	}
	if cfg.GitlabToken == "" {
		log.Fatal("Env var AM_GITLAB_TOKEN is not set, nor \"gitlab.token\" in config file.")
	}

	return cfg
}

func (c Config) GetEnvironments() map[string]Environment {
	confedEnvs := viper.GetStringMap("environments")

	envs := make(map[string]Environment)
	for k := range confedEnvs {
		envs[k] = Environment{
			Description: viper.GetString("environments." + k + ".description"),
			Cluster:     viper.GetString("environments." + k + ".cluster"),
		}
	}
	return envs
}

func (c Config) GetEnvironmentNames() []string {
	confedEnvs := viper.GetStringMap("environments")

	envNames := make([]string, 0)
	for k := range confedEnvs {
		envNames = append(envNames, k)
	}
	return envNames
}

func setDefaultValues() {
	viper.SetDefault(argocdConfigNamespace, "openshift-gitops")
	viper.SetDefault(logRequests, false)
}

func bindEnvironmentVariables() {
	viper.SetEnvPrefix("am")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.BindEnv(gitlabTokenKey)
	viper.BindEnv(aardvarkWebhookUrlKey)
	viper.BindEnv(aardvarkWebhookSecretKey)
	viper.BindEnv(gitUserEmailKey)
}

func readConfigFile() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("/etc/aardvarkmanager/")
	viper.AddConfigPath("$HOME/.aardvarkmanager")
	viper.AddConfigPath(".")

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// Config file not found; ignore error if desired
		} else {
			panic(fmt.Errorf("fatal error config file: %w", err))
		}
	}
}
