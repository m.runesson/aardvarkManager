package ping

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type Config struct {
}

func (c Config) Ping(ctx *gin.Context) {
	ctx.String(http.StatusOK, "Pong!")
}
