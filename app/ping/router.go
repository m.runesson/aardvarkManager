package ping

import "github.com/gin-gonic/gin"

// Pingable interface declaring methods of the ping controller
type Pingable interface {
	Ping(ctx *gin.Context)
}

// Routes ping to the given gin.Engine
func Routes(router *gin.RouterGroup, pingable Pingable) {
	router.GET("/ping", pingable.Ping)
}
