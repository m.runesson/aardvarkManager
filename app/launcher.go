package app

import (
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/app/argocd"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/app/environment"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/app/me"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/app/ping"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/app/project"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/config"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/internal/configGenerator"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/internal/gitlab"
	kubernetesService "gitlab.com/arbetsformedlingen/devops/aardvarkManager/internal/kubernetes"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/internal/namespaceService"
	"net/http/httputil"
	"time"
)

func Launch(config config.Config, gitService gitlab.GitProjectHost) {
	var err error
	kubernetesService := kubernetesService.CreateService()

	engine := gin.Default()

	engine.Use(cors.New(cors.Config{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"POST", "GET", "OPTIONS"},
		AllowHeaders:     []string{"Origin", "content-type", "accept"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           9 * time.Hour,
	}))
	if config.LogRequests {
		engine.Use(RequestLogger())
	}

	ping.Routes(&engine.RouterGroup, ping.Config{})

	apiV1Router := engine.Group("api/v1")
	project.Routes(apiV1Router, project.Config{
		Gitlab:                gitService,
		AardvarkWebHook:       config.AardvarkWebhookUrl,
		AardvarkWebHookSecret: config.AardvarkWebhookSecret,
		NamespaceService: namespaceService.CreateService(namespaceService.NamespaceServiceOptions{
			GitlabService:       gitService,
			NamespaceRepository: config.NamespaceConfigProject,
			Environments:        config.GetEnvironments(),
		}),
		EnvironmentNames:  config.GetEnvironmentNames(),
		KubernetesService: kubernetesService,
		ConfigGenerator:   configGenerator.CreateService(config.GetEnvironments(), gitService, config.ArgocdConfigProject),
	})
	environment.Routes(apiV1Router, environment.Config{
		Environments: config.GetEnvironments(),
	})
	me.Routes(apiV1Router, me.Config{})

	argocd.Routes(apiV1Router, argocd.Config{
		KubernetesService: kubernetesService,
		ArgocdNamespace:   config.ArgocdConfigNamespace})

	err = engine.Run()
	if err != nil {
		panic(fmt.Errorf("failed to listen to port: %w", err))
	}
}

func RequestLogger() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		fmt.Println(ctx.Request.Host, ctx.Request.RemoteAddr, ctx.Request.RequestURI)

		// Save a copy of this request for debugging.
		requestDump, err := httputil.DumpRequest(ctx.Request, true)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(string(requestDump))

		ctx.Next()
	}
}
