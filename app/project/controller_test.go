package project

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/config"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/internal/namespaceService"
	"testing"
)

func TestConvertRequestEnvironmentsToNamespaces(t *testing.T) {
	namespaces := convertRequestEnvironmentsToNamespaces(
		postRecord{
			Project:      "",
			Environments: []string{"foo", "bar"},
			OwnerTeam:    "myteam",
		},
		"myproject",
		"")
	assert.Equal(t, 2, len(namespaces))
	assert.Equal(t, namespaceService.Namespace{
		Name:        "myproject-foo",
		User:        "",
		Team:        "myteam",
		Labels:      map[string]string{"argocd.argoproj.io/managed-by": "openshift-gitops"},
		Annotations: nil,
		Environment: "foo",
	}, namespaces[0])
}

func TestConvertRequestEnvironmentsToNamespacesDevelopHasNoLabels(t *testing.T) {
	namespaces := convertRequestEnvironmentsToNamespaces(
		postRecord{
			Project:      "",
			Environments: []string{"develop"},
			OwnerTeam:    "myteam",
		},
		"myproject",
		"")
	assert.Equal(t,
		namespaceService.Namespace{
			Name:        "myproject-develop",
			User:        "",
			Team:        "myteam",
			Labels:      map[string]string{},
			Annotations: nil,
			Environment: "develop",
		},
		namespaces[0])
}

func TestValidateRequestDataWithCorrectData(t *testing.T) {
	config := Config{
		EnvironmentNames:  []string{"foo", "bar"},
		KubernetesService: KubernetesServiceMock{},
	}
	post := postRecord{
		Project:      "gazonk/myProject",
		Environments: []string{"foo"},
	}
	assert.Nil(t, config.validateRequestData(post, "myuser", []string{"group1"}))
}

func TestValidateRequestDataWithUnknownEnvironment(t *testing.T) {
	config := Config{
		EnvironmentNames:  []string{"foo", "bar"},
		KubernetesService: KubernetesServiceMock{},
	}
	post := postRecord{
		Project:      "gazonk/myProject",
		Environments: []string{"gazonk"},
	}
	assert.Equal(t,
		"unknown environment gazonk",
		config.validateRequestData(post, "myuser", []string{"group1"}).Error())
}

func TestValidateRequestDataWithDuplicateEnvironment(t *testing.T) {
	config := Config{
		EnvironmentNames:  []string{"foo", "bar"},
		KubernetesService: KubernetesServiceMock{},
	}
	post := postRecord{
		Project:      "gazonk/myProject",
		Environments: []string{"foo", "foo"},
	}
	assert.Equal(t,
		"environments must be unique",
		config.validateRequestData(post, "myuser", []string{"group1"}).Error())
}

func TestValidateRequestDataWithNoEnvironment(t *testing.T) {
	config := Config{
		EnvironmentNames:  []string{"foo", "bar"},
		KubernetesService: KubernetesServiceMock{},
	}
	post := postRecord{
		Project:      "gazonk/myProject",
		Environments: []string{},
	}
	assert.Equal(t,
		"at least one environment must be provided",
		config.validateRequestData(post, "myuser", []string{"group1"}).Error())
}

func TestValidateRequestDataWithNoProject(t *testing.T) {
	config := Config{
		EnvironmentNames:  []string{"foo", "bar"},
		KubernetesService: KubernetesServiceMock{},
	}
	post := postRecord{
		Project:      "",
		Environments: []string{"foo"},
	}
	assert.Equal(t,
		"project must be provided",
		config.validateRequestData(post, "myuser", []string{"group1"}).Error())
}

func TestValidateRequestDataUserNotMemberOfFroup(t *testing.T) {
	config := Config{
		EnvironmentNames:  []string{"foo", "bar"},
		KubernetesService: KubernetesServiceMock{},
	}
	post := postRecord{
		Project:      "gazonk/myProject",
		Environments: []string{"foo"},
		OwnerTeam:    "group2",
	}
	assert.Equal(t,
		"myuser is not member of group2",
		config.validateRequestData(post, "myuser", []string{"group1"}).Error())
}

func TestValidateRequestDataUserIsMemberOfGroup(t *testing.T) {
	config := Config{
		EnvironmentNames:  []string{"foo", "bar"},
		KubernetesService: KubernetesServiceMock{},
	}
	post := postRecord{
		Project:      "gazonk/myProject",
		Environments: []string{"foo"},
		OwnerTeam:    "group1",
	}
	assert.Nil(t, config.validateRequestData(post, "myuser", []string{"group1"}))
}

func TestValidateRequestDataUserIsAdminAndCanHaveAnyTeam(t *testing.T) {
	cfg := Config{
		EnvironmentNames:  []string{"foo", "bar"},
		KubernetesService: KubernetesServiceMock{},
	}
	post := postRecord{
		Project:      "gazonk/myProject",
		Environments: []string{"foo"},
		OwnerTeam:    "group1",
	}
	assert.Nil(t,
		cfg.validateRequestData(post, "myuser", []string{"group2", config.AdminGroup}))
}

func TestValidateRequestDataUserIsAdminAndProvideNonExistingTeam(t *testing.T) {
	cfg := Config{
		EnvironmentNames:  []string{"foo", "bar"},
		KubernetesService: KubernetesServiceMock{},
	}
	post := postRecord{
		Project:      "gazonk/myProject",
		Environments: []string{"foo"},
		OwnerTeam:    "group2",
	}
	assert.Equal(t,
		"group2 does not exist",
		cfg.validateRequestData(post, "myuser", []string{"group2", config.AdminGroup}).Error())
}

type KubernetesServiceMock struct {
	mock.Mock
}

func (k KubernetesServiceMock) IsExistingGroup(groupName string) bool {
	return groupName == "group1"
}

func TestGetLastLevelOfPathWithMultipleLevels(t *testing.T) {
	assert.Equal(t, "gazonk", getLastLevelOfPath("/foo/bar/gazonk"))
}

func TestGetLastLevelOfPathWithNoLevels(t *testing.T) {
	assert.Equal(t, "gazonk", getLastLevelOfPath("gazonk"))
}
