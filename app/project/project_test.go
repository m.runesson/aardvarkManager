package project

import (
	"github.com/gin-gonic/gin"
	"github.com/go-git/go-git/v5/plumbing/transport"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	gl "github.com/xanzy/go-gitlab"
	gitlab "gitlab.com/arbetsformedlingen/devops/aardvarkManager/internal/gitlab"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
)

var fixture struct {
	engine *gin.Engine
	config Config
}

type GitMock struct {
	mock.Mock
}

type GitProjectHostMock struct {
	mock.Mock
}

func (g GitProjectHostMock) GetGitLabClient() *gl.Client {
	//TODO implement me
	panic("implement me")
}

func (g GitProjectHostMock) GetUserInfo() (string, string) {
	//TODO implement me
	panic("implement me")
}

func (g GitProjectHostMock) GetGitAuth() transport.AuthMethod {
	//TODO implement me
	panic("implement me")
}

func (g GitProjectHostMock) GetProject(repositoryUrl string) (gitlab.Project, error) {
	//TODO implement me
	panic("implement me")
}

func (g GitProjectHostMock) CreateProject(options gitlab.CreateProjectOptions) (gitlab.Project, error) {
	//TODO implement me
	panic("implement me")
}

func (m GitMock) GetGitLabClient() *gl.Client {
	//TODO implement me
	panic("implement me")
}

func (m GitMock) GetUserInfo() (string, string) {
	//TODO implement me
	panic("implement me")
}

func (m GitMock) GetGitAuth() transport.AuthMethod {
	//TODO implement me
	panic("implement me")
}

func (m GitMock) GetProject(repositoryUrl string) (gitlab.Project, error) {
	return gitlab.Project{
		Id:            42,
		Name:          "MyProject",
		RepoUrl:       "gitProject@gitlab.com/org/MyProject.gitProject",
		DefaultBranch: "main",
		Service:       GitProjectHostMock{},
	}, nil
}

func (m GitMock) CreateProject(options gitlab.CreateProjectOptions) (gitlab.Project, error) {
	return gitlab.Project{}, nil
}

func (m GitMock) CreateHook(project gitlab.Project, webHook gitlab.WebHook) error {
	return nil
}

func setup() {
	fixture.engine = gin.Default()

	fixture.config = Config{
		Gitlab:                GitMock{},
		AardvarkWebHook:       "http://NOT.REAL/",
		AardvarkWebHookSecret: "Not so secret",
	}
	Routes(&fixture.engine.RouterGroup, fixture.config)
}

func TestPostProject(t *testing.T) {
	t.SkipNow() // Need fix better gitlab mocking.
	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPost, "/project", strings.NewReader("{\"project\": \"arbetsformedlingen/devops/aardvark-demo\"}"))
	req.Header.Add("Content-Type", "application/json")
	fixture.engine.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, "{\"Project\":{\"Id\":42,\"NamespaceId\":0,\"Name\":\"MyProject\",\"Path\":\"\",\"RepoUrl\":\"gitProject@gitlab.com/org/MyProject.gitProject\",\"DefaultBranch\":\"main\"}}", w.Body.String())
}

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	os.Exit(code)
}
