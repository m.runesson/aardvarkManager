package project

import "github.com/gin-gonic/gin"

type Controllable interface {
	CreateProject(c *gin.Context)
}

func Routes(router *gin.RouterGroup, controllable Controllable) {
	router.POST("/project", controllable.CreateProject)
}
