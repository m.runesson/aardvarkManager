package project

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/config"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/internal/configGenerator"
	ginUtil "gitlab.com/arbetsformedlingen/devops/aardvarkManager/internal/ginutil"
	gitlab "gitlab.com/arbetsformedlingen/devops/aardvarkManager/internal/gitlab"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/internal/namespaceService"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/internal/stringutil"
	"net/http"
	"strings"
)

type Config struct {
	Gitlab                gitlab.GitProjectHost
	AardvarkWebHook       string
	AardvarkWebHookSecret string
	NamespaceService      namespaceServiceInterface
	EnvironmentNames      []string
	KubernetesService     kubernetesServiceInterface
	ConfigGenerator       configGeneratorServiceInterface
}

type postRecord struct {
	Project      string
	Environments []string
	OwnerTeam    string `json:"owner_team"`
}

type responseRecord struct {
	SrcProject            gitlab.GitProject `json:"src_project"`
	InfraProject          gitlab.GitProject `json:"infra_project"`
	NamespaceMergeRequest string            `json:"merge_request_namespaces"`
	ArgocdMergeRequest    string            `json:"merge_request_argocd"`
	Message               string            `json:"message,omitempty"`
}

type namespaceServiceInterface interface {
	AddNamespaces(namespaces ...namespaceService.Namespace) (string, error)
}

type kubernetesServiceInterface interface {
	IsExistingGroup(groupName string) bool
}

type configGeneratorServiceInterface interface {
	AddArgoCDApplications(projectName string, environments []string, ownerTeam string, infraProjectGitUrl string) (string, error)
}

func (c Config) CreateProject(ctx *gin.Context) {
	var requestData postRecord
	response := responseRecord{}
	err := ctx.Bind(&requestData)
	if err != nil {
		response.Message = err.Error()
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	err = c.validateRequestData(requestData, ginUtil.WhoAmI(ctx), ginUtil.GetMyGroups(ctx))
	if err != nil {
		response.Message = err.Error()
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	project, err := c.Gitlab.GetProject(requestData.Project)
	if err != nil {
		response.Message = fmt.Sprintf("Cannot get project %s: %v", requestData.Project, err)
		ctx.JSON(http.StatusNotFound, response)
		return
	}
	response.SrcProject = project

	if !project.HasFile("Dockerfile") {
		response.Message = "Dockerfile not found in repository, nothing done."
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	err = project.CreateHook(gitlab.WebHook{
		Url:                 c.AardvarkWebHook,
		Secret:              c.AardvarkWebHookSecret,
		PushEvents:          true,
		MergeRequestsEvents: true,
		TagPushEvents:       true,
	})
	if err != nil {
		response.Message = err.Error()
		ctx.JSON(http.StatusInternalServerError, response)
		return
	}

	response.InfraProject, err = c.setupInfraRepository(project, requestData.Environments)
	if err != nil {
		response.Message = err.Error()
		ctx.JSON(http.StatusInternalServerError, response)
		return
	}

	namespacesMR, err := c.addDesiredNamespaces(ctx, requestData, project)
	response.NamespaceMergeRequest = namespacesMR
	if err != nil {
		response.Message = fmt.Sprintf("Failed to configure namespaces: %v", err)
		ctx.JSON(http.StatusInternalServerError, response)
		return
	}

	argocdMR, err := c.ConfigGenerator.AddArgoCDApplications(
		project.Name,
		requestData.Environments,
		requestData.OwnerTeam,
		response.InfraProject.GetRepositoryHttpUrl())
	if err != nil {
		response.Message = fmt.Sprintf("Failed to configure ArgoCD applications: %v", err)
		ctx.JSON(http.StatusInternalServerError, response)
		return
	}
	response.ArgocdMergeRequest = argocdMR

	ctx.JSON(http.StatusOK, response)
}

func (c Config) addDesiredNamespaces(ctx *gin.Context, requestData postRecord, project gitlab.Project) (string, error) {
	user := ""
	if requestData.OwnerTeam == "" {
		user = ginUtil.WhoAmI(ctx)
	}

	namespaces := convertRequestEnvironmentsToNamespaces(requestData, project.Name, user)
	return c.NamespaceService.AddNamespaces(namespaces...)
}

func convertRequestEnvironmentsToNamespaces(requestData postRecord, projectName string, user string) []namespaceService.Namespace {
	namespaces := make([]namespaceService.Namespace, len(requestData.Environments))
	for i := range requestData.Environments {
		labels := make(map[string]string)
		if requestData.Environments[i] != "develop" {
			labels["argocd.argoproj.io/managed-by"] = "openshift-gitops"
		}
		namespaces[i] = namespaceService.Namespace{
			Name:        projectName + "-" + requestData.Environments[i],
			User:        user,
			Team:        requestData.OwnerTeam,
			Labels:      labels,
			Environment: requestData.Environments[i],
		}
	}
	return namespaces
}

func (c Config) validateRequestData(record postRecord, userName string, myGroups []string) error {
	if stringutil.IsMemberOf(config.AdminGroup, myGroups) && record.OwnerTeam != "" {
		if !c.KubernetesService.IsExistingGroup(record.OwnerTeam) {
			return errors.New(record.OwnerTeam + " does not exist")
		}
	} else {
		if record.OwnerTeam != "" && !stringutil.IsMemberOf(record.OwnerTeam, myGroups) {
			return errors.New(userName + " is not member of " + record.OwnerTeam)
		}
	}

	if record.Project == "" {
		return errors.New("project must be provided")
	}

	if len(record.Environments) < 1 {
		return errors.New("at least one environment must be provided")
	}

	if !stringutil.HasOnlyUniqueStrings(record.Environments) {
		return errors.New("environments must be unique")
	}

	for _, e := range record.Environments {
		if !stringutil.IsMemberOf(e, c.EnvironmentNames) {
			return errors.New("unknown environment " + e)
		}
	}
	return nil
}

func (c Config) setupInfraRepository(sourceProject gitlab.Project, environments []string) (gitlab.Project, error) {
	infraProject, err := c.Gitlab.GetProject(sourceProject.Path + "-infra")
	if err != nil {
		options := gitlab.CreateProjectOptions{
			Name:          sourceProject.Name + " infra",
			NamespaceId:   sourceProject.NamespaceId,
			Path:          getLastLevelOfPath(sourceProject.Path) + "-infra",
			Description:   "Infrastructure code for " + sourceProject.Name,
			DefaultBranch: sourceProject.DefaultBranch,
			Visibility:    sourceProject.Visibility,
		}
		infraProject, err = c.Gitlab.CreateProject(options)
		if err != nil {
			return gitlab.Project{}, fmt.Errorf("cannot create project %s: %w", sourceProject.Path+"-infra", err)
		}
	}

	err = infraProject.CreateHook(gitlab.WebHook{
		Url:                 c.AardvarkWebHook,
		Secret:              c.AardvarkWebHookSecret,
		PushEvents:          true,
		MergeRequestsEvents: true,
		TagPushEvents:       true,
	})
	if err != nil {
		return infraProject, fmt.Errorf("cannot create hook for project %s: %w", sourceProject.Path+"-infra", err)
	}

	if !infraProject.HasFile("kustomize") {
		repo, err := infraProject.CloneGitRepository()
		if err != nil {
			return infraProject, err
		}
		err = configGenerator.CreateKustomize(repo, getLastLevelOfPath(sourceProject.Path), environments)
		if err != nil {
			return infraProject, err
		}
		_, err = repo.CommitAndPush("Initial Kustomize file structure.")
		if err != nil {
			return infraProject, err
		}
	}

	return infraProject, nil
}

func getLastLevelOfPath(path string) string {
	arr := strings.Split(path, "/")
	return arr[len(arr)-1]
}
