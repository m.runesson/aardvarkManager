package me

import (
	"github.com/gin-gonic/gin"
	ginUtil "gitlab.com/arbetsformedlingen/devops/aardvarkManager/internal/ginutil"
	"net/http"
)

type Config struct {
}

type meResponseRecord struct {
	UserId string   `json:"user_id"`
	Groups []string `json:"Groups"`
}

func (c Config) GetInfoAboutMe(ctx *gin.Context) {
	responseRecord := meResponseRecord{
		UserId: ginUtil.WhoAmI(ctx),
		Groups: ginUtil.GetMyGroups(ctx),
	}
	if responseRecord.UserId == "" {
		ctx.String(http.StatusNotFound, "No user information. "+
			"Are you really behind a authenticating proxy?")
	} else {
		ctx.JSON(http.StatusOK, responseRecord)
	}
}
