package me

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	ginUtil "gitlab.com/arbetsformedlingen/devops/aardvarkManager/internal/ginutil"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
)

const test_user = "myuser"
const test_groups = "group1,group2"

var fixture struct {
	engine *gin.Engine
}

func TestGetMeWithoutUserHeader(t *testing.T) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodGet, "/me", nil)
	fixture.engine.ServeHTTP(w, req)

	assert.Equal(t, http.StatusNotFound, w.Code)
	assert.Equal(t, "No user information. Are you really behind a authenticating proxy?", w.Body.String())
}

func TestGetMeWithFullUserInfo(t *testing.T) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodGet, "/me", nil)
	req.Header.Set(ginUtil.UserHeader, test_user)
	req.Header.Set(ginUtil.GroupHeader, test_groups)
	fixture.engine.ServeHTTP(w, req)

	var responseData meResponseRecord
	json.Unmarshal(w.Body.Bytes(), &responseData)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, test_user, responseData.UserId)
	assert.Equal(t, strings.Split(test_groups, ","), responseData.Groups)
}

func TestGetMeWithNoGroupsButHasHeader(t *testing.T) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodGet, "/me", nil)
	req.Header.Set(ginUtil.UserHeader, test_user)
	req.Header.Set(ginUtil.GroupHeader, "")
	fixture.engine.ServeHTTP(w, req)

	var responseData meResponseRecord
	json.Unmarshal(w.Body.Bytes(), &responseData)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, test_user, responseData.UserId)
	assert.Empty(t, responseData.Groups)
}

func TestGetMeWithNoGroupsAndNoGroupHeader(t *testing.T) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodGet, "/me", nil)
	req.Header.Set(ginUtil.UserHeader, test_user)
	fixture.engine.ServeHTTP(w, req)

	var responseData meResponseRecord
	json.Unmarshal(w.Body.Bytes(), &responseData)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, test_user, responseData.UserId)
	assert.Empty(t, responseData.Groups)
}

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	os.Exit(code)
}

func setup() {
	fixture.engine = gin.Default()
	Routes(&fixture.engine.RouterGroup, Config{})
}
