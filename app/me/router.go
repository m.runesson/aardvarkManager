package me

import "github.com/gin-gonic/gin"

type Controllable interface {
	GetInfoAboutMe(c *gin.Context)
}

func Routes(router *gin.RouterGroup, controllable Controllable) {
	router.GET("/me", controllable.GetInfoAboutMe)
}
