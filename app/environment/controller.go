package environment

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/config"
	"net/http"
)

type Config struct {
	Environments map[string]config.Environment
}

type responseRecord struct {
	Environments []environmentRecord `json:"environments"`
}

type environmentRecord struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

func (c Config) GetAllEnvironments(ctx *gin.Context) {
	resultEnvs := make([]environmentRecord, 0, len(c.Environments))
	for k := range c.Environments {
		resultEnvs = append(resultEnvs, environmentRecord{
			Name:        k,
			Description: c.Environments[k].Description,
		})
	}
	result := responseRecord{
		Environments: resultEnvs,
	}
	ctx.JSON(http.StatusOK, result)
}
