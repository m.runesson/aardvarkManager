package environment

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/config"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

var fixture struct {
	engine *gin.Engine
}

func TestGetEnvironments(t *testing.T) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodGet, "/environments", nil)
	fixture.engine.ServeHTTP(w, req)

	var responseData responseRecord
	json.Unmarshal(w.Body.Bytes(), &responseData)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, 2, len(responseData.Environments))
	assert.Contains(t, responseData.Environments, environmentRecord{Name: "foo", Description: "foo is foo"})
	assert.Contains(t, responseData.Environments, environmentRecord{Name: "bar", Description: "foo is foo"})
}

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	os.Exit(code)
}

func setup() {
	fixture.engine = gin.Default()
	envs := map[string]config.Environment{
		"foo": {Description: "foo is foo"},
		"bar": {Description: "foo is foo"},
	}
	Routes(&fixture.engine.RouterGroup, Config{
		Environments: envs,
	})
}
