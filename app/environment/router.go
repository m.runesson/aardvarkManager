package environment

import "github.com/gin-gonic/gin"

type Controllable interface {
	GetAllEnvironments(c *gin.Context)
}

func Routes(router *gin.RouterGroup, controllable Controllable) {
	router.GET("/environments", controllable.GetAllEnvironments)
}
