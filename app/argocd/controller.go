package argocd

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/config"
	ginUtil "gitlab.com/arbetsformedlingen/devops/aardvarkManager/internal/ginutil"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/internal/stringutil"
	"net/http"
)

type Config struct {
	KubernetesService kubernetesServiceInterface
	ArgocdNamespace   string
}

type kubernetesServiceInterface interface {
	GetAllAppProjectNames(namespace string) []string
}

type responseRecord struct {
	Groups []groupRecord `json:"groups"`
}

type groupRecord struct {
	Name string `json:"name"`
}

func (c Config) GetAllowedOwners(ctx *gin.Context) {
	appProjects := c.KubernetesService.GetAllAppProjectNames(c.ArgocdNamespace)
	myGroups := ginUtil.GetMyGroups(ctx)
	ctx.JSON(http.StatusOK, responseRecord{
		Groups: getMyMemberGroups(myGroups, appProjects),
	})
}

func getMyMemberGroups(myGroups []string, appProjects []string) []groupRecord {
	admin := stringutil.IsMemberOf(config.AdminGroup, myGroups)
	var groupRecords []groupRecord
	groupRecords = []groupRecord{}
	for _, p := range appProjects {
		if admin || stringutil.IsMemberOf(p, myGroups) {
			groupRecords = append(groupRecords, groupRecord{p})
		}
	}
	return groupRecords
}
