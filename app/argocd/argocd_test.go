package argocd

import (
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/config"
	"testing"
)

var fixture struct {
	engine *gin.Engine
}

func TestGetMyMemberGroupsForAdminReturnAllGroups(t *testing.T) {
	appProjects := []string{"foo", "bar", "gazonk"}
	myGroups := []string{"foo", config.AdminGroup}
	expected := []groupRecord{
		{Name: "foo"},
		{Name: "bar"},
		{Name: "gazonk"},
	}
	assert.Equal(t, expected, getMyMemberGroups(myGroups, appProjects))
}

func TestGetMyMemberGroupsForNonAdminReturnsOnlyMyGroups(t *testing.T) {
	appProjects := []string{"foo", "bar", "gazonk"}
	myGroups := []string{"foo", "gazonk"}
	expected := []groupRecord{
		{Name: "foo"},
		{Name: "gazonk"},
	}
	assert.Equal(t, expected, getMyMemberGroups(myGroups, appProjects))
}
