package argocd

import "github.com/gin-gonic/gin"

type Controllable interface {
	GetAllowedOwners(c *gin.Context)
}

func Routes(router *gin.RouterGroup, controllable Controllable) {
	router.GET("/allowedowners", controllable.GetAllowedOwners)
}
