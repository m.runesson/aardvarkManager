package namespaceService

import (
	"errors"
	"fmt"
	gitlab2 "github.com/xanzy/go-gitlab"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/config"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/internal/gitlab"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/internal/memfsutil"
	"log"
	"time"
)

type NamespaceServiceOptions struct {
	GitlabService       gitlabService
	NamespaceRepository string
	Environments        map[string]config.Environment
}

type serviceConfig struct {
	namespaceProject gitlab.Project
	environments     map[string]config.Environment
}

type gitlabService interface {
	GetProject(repositoryUrl string) (gitlab.Project, error)
}

func CreateService(options NamespaceServiceOptions) serviceConfig {
	project, err := options.GitlabService.GetProject(options.NamespaceRepository)
	if err != nil {
		log.Fatalln("Unknown Namespace repository "+options.NamespaceRepository, err)
	}
	return serviceConfig{
		namespaceProject: project,
		environments:     options.Environments,
	}
}

func (s serviceConfig) AddNamespaces(namespaces ...Namespace) (string, error) {
	if namespaces == nil || len(namespaces) == 0 {
		return "", errors.New("no namespaces provided")
	}
	branchName := fmt.Sprintf("am-%s-%d", namespaces[0].Name, time.Now().UnixNano())
	repository, err := s.namespaceProject.CloneGitRepository()
	if err != nil {
		return "", err
	}
	err = repository.NewBranch(branchName)
	if err != nil {
		return "", err
	}
	// Add namespaces
	fs := *repository.GetFileSystem()
	commitMsg := "Adding namespaces "
	for n := range namespaces {
		filePath := "kustomize/overlays/" + s.environments[namespaces[n].Environment].Cluster + "/namespaces.yaml"
		data, err := memfsutil.ReadFile(fs, filePath)
		if err != nil {
			return "", err
		}
		changedData, err := addNamespaceToFile([]byte(data), namespaces[n])
		if err != nil {
			return "", err
		}
		err = memfsutil.WriteFile(fs, filePath, changedData)
		if err != nil {
			return "", err
		}
		err = repository.Add(filePath)
		if err != nil {
			return "", err
		}
		commitMsg += namespaces[n].Name + " "
	}
	_, err = repository.CommitAndPush(commitMsg)
	if err != nil {
		return "", err
	}

	removeSrcBranch := true
	squash := false
	collaboration := false
	mr, err := s.namespaceProject.CreateMergeRequest(gitlab2.CreateMergeRequestOptions{
		Title:              &commitMsg,
		Description:        &commitMsg,
		SourceBranch:       &branchName,
		TargetBranch:       &s.namespaceProject.DefaultBranch,
		RemoveSourceBranch: &removeSrcBranch,
		Squash:             &squash,
		AllowCollaboration: &collaboration,
	})
	if err != nil {
		return mr.WebURL, err
	}
	return mr.WebURL, nil
}

func addNamespaceToFile(data []byte, namespace Namespace) ([]byte, error) {
	namespacesFile, err := UnmarshalToNamespaces(data)
	if err != nil {
		return nil, err
	}
	err = namespacesFile.AddNamespace(namespace)
	if err != nil {
		return nil, err
	}
	return namespacesFile.MarshalNamespaces()
}
