package namespaceService

import (
	"github.com/stretchr/testify/assert"
	"gopkg.in/yaml.v3"
	"testing"
)

const yaml_one_namespace = `
namespaces:
- name: aardvark
  team: calamari
  labels:
    argocd.argoproj.io/managed-by: openshift-gitops
  annotations:
    foo: bar
`

func TestUnmarshalNamespaces(t *testing.T) {
	ns, err := UnmarshalToNamespaces([]byte(yaml_one_namespace))
	assert.Nil(t, err)
	assert.Equal(t, "aardvark", ns.Namespaces[0].Name)
	assert.Equal(t, "calamari", ns.Namespaces[0].Team)
	assert.Equal(t, "openshift-gitops", ns.Namespaces[0].Labels["argocd.argoproj.io/managed-by"])
	assert.Equal(t, "bar", ns.Namespaces[0].Annotations["foo"])
	assert.Equal(t, 1, len(ns.Namespaces))
}

func TestUnmarshalToNamespacesWrongYaml(t *testing.T) {
	data := "foo: bar"
	ns, err := UnmarshalToNamespaces([]byte(data))
	assert.Nil(t, err)
	assert.Equal(t, 0, len(ns.Namespaces))
}

func TestUnmarshalToNamespacesNotYaml(t *testing.T) {
	data := "foo; bar"
	_, err := UnmarshalToNamespaces([]byte(data))
	assert.NotNil(t, err)
}

func TestHasNamespaceTrue(t *testing.T) {
	ns, _ := UnmarshalToNamespaces([]byte(yaml_one_namespace))
	assert.True(t, ns.hasNamespace("aardvark"))
}

func TestHasNamespaceFalse(t *testing.T) {
	ns, _ := UnmarshalToNamespaces([]byte(yaml_one_namespace))
	assert.False(t, ns.hasNamespace("not-aardvark"))
}

func TestAddNamespaceAlreadyExisting(t *testing.T) {
	ns, _ := UnmarshalToNamespaces([]byte(yaml_one_namespace))
	err := ns.AddNamespace(Namespace{
		Name: "aardvark",
		User: "myuser",
	})
	assert.NotNil(t, err)
}

func TestAddNamespaceWithoutUserOrTeam(t *testing.T) {
	ns, _ := UnmarshalToNamespaces([]byte(yaml_one_namespace))
	err := ns.AddNamespace(Namespace{
		Name: "aardvark",
	})
	assert.NotNil(t, err)
}

func TestAddNamespaceWithBothUserAndTeam(t *testing.T) {
	ns, _ := UnmarshalToNamespaces([]byte(yaml_one_namespace))
	err := ns.AddNamespace(Namespace{
		Name: "aardvark",
		User: "myuser",
		Team: "myteam",
	})
	assert.NotNil(t, err)
}

func TestAddNamespaceAppearInAlphabeticOrderPositionIsInMiddle(t *testing.T) {
	underTest := namespaces{
		Namespaces: []Namespace{
			{
				Name: "a",
			},
			{
				Name: "c",
			},
		},
	}
	newNamespace := Namespace{Name: "b", User: "myuser"}
	underTest.AddNamespace(newNamespace)
	assert.Equal(t,
		namespaces{
			Namespaces: []Namespace{
				{
					Name: "a",
				},
				{
					Name: "b",
					User: "myuser",
				},
				{
					Name: "c",
				},
			},
		},
		underTest)
}

func TestAddNamespaceAppearInAlphabeticOrderPositionIsFirst(t *testing.T) {
	underTest := namespaces{
		Namespaces: []Namespace{
			{
				Name: "c",
			},
		},
	}
	newNamespace := Namespace{Name: "a", User: "myuser"}
	underTest.AddNamespace(newNamespace)
	assert.Equal(t,
		namespaces{
			Namespaces: []Namespace{
				{
					Name: "a",
					User: "myuser",
				},
				{
					Name: "c",
				},
			},
		},
		underTest)
}

func TestAddNamespaceAppearInAlphabeticOrderPositionLast(t *testing.T) {
	underTest := namespaces{
		Namespaces: []Namespace{
			{
				Name: "c",
			},
		},
	}
	newNamespace := Namespace{Name: "d", User: "myuser"}
	underTest.AddNamespace(newNamespace)
	assert.Equal(t,
		namespaces{
			Namespaces: []Namespace{
				{
					Name: "c",
				},
				{
					Name: "d",
					User: "myuser",
				},
			},
		},
		underTest)
}

func TestMarshalNamespaces(t *testing.T) {
	underTest := namespaces{
		Namespaces: []Namespace{
			{
				Name:        "a-name",
				User:        "a-user",
				Team:        "a-team",
				Labels:      map[string]string{"foo": "bar"},
				Annotations: map[string]string{"ann": "gazonk"},
				Environment: "Shall-not-render",
			},
		},
	}
	data, err := yaml.Marshal(underTest)
	assert.Nil(t, err)
	assert.Equal(t,
		"namespaces:\n    - name: a-name\n      user: a-user\n      team: a-team\n      labels:\n        foo: bar\n      annotations:\n        ann: gazonk\n",
		string(data))
}
