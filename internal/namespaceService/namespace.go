package namespaceService

import (
	"errors"
	"gopkg.in/yaml.v3"
)

type namespaces struct {
	Namespaces []Namespace
}

type Namespace struct {
	Name        string
	User        string            `yaml:"user,omitempty"`
	Team        string            `yaml:"team,omitempty"`
	Labels      map[string]string `yaml:"labels,omitempty"`
	Annotations map[string]string `yaml:"annotations,omitempty"`
	Environment string            `yaml:"-"`
}

func UnmarshalToNamespaces(data []byte) (*namespaces, error) {
	namespaces := namespaces{}
	err := yaml.Unmarshal(data, &namespaces)
	if err != nil {
		return nil, err
	}
	return &namespaces, nil
}

func (n *namespaces) MarshalNamespaces() ([]byte, error) {
	return yaml.Marshal(n)
}

func (n *namespaces) hasNamespace(ns string) bool {
	for i := range n.Namespaces {
		if n.Namespaces[i].Name == ns {
			return true
		}
	}
	return false
}

func (n *namespaces) AddNamespace(ns Namespace) error {
	if ns.User == "" && ns.Team == "" {
		return errors.New("one of user or team must exist")
	}
	if ns.User != "" && ns.Team != "" {
		return errors.New("both user and team cannot exist")
	}
	if n.hasNamespace(ns.Name) {
		return errors.New("namespace " + ns.Name + " already exist")
	}
	for i := range n.Namespaces {
		if n.Namespaces[i].Name > ns.Name {
			last := len(n.Namespaces) - 1
			n.Namespaces = append(n.Namespaces, n.Namespaces[last])
			copy(n.Namespaces[i+1:], n.Namespaces[i:last])
			n.Namespaces[i] = ns
			return nil
		}
	}
	n.Namespaces = append(n.Namespaces, ns)
	return nil
}
