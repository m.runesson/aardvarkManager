package ginUtil

import (
	"github.com/gin-gonic/gin"
	"strings"
)

const UserHeader = "X-Forwarded-Email"
const GroupHeader = "X-Forwarded-Groups"

func WhoAmI(ctx *gin.Context) string {
	return ctx.GetHeader(UserHeader)
}

func GetMyGroups(ctx *gin.Context) []string {
	groupString := ctx.GetHeader(GroupHeader)
	if groupString == "" {
		return []string{}
	} else {
		return strings.Split(groupString, ",")
	}
}
