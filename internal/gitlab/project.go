package gitlab

import (
	"errors"
	"fmt"
	"github.com/go-git/go-billy/v5/memfs"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/storage/memory"
	"github.com/xanzy/go-gitlab"
	"log"
	"strings"
)

type Project struct {
	Id            int                    `json:"project_id"`
	NamespaceId   int                    `json:"namespace_id"`
	Name          string                 `json:"name"`
	Path          string                 `json:"path"`
	RepoUrl       string                 `json:"http_repo_url"`
	DefaultBranch string                 `json:"default_branch"`
	Visibility    gitlab.VisibilityValue `json:"-"`
	Service       GitProjectHost         `json:"-"`
}

func (p Project) GetRepositoryHttpUrl() string {
	return p.RepoUrl
}

func (p Project) HasFile(filePath string) bool {
	_, _, err := p.Service.GetGitLabClient().RepositoryFiles.GetFileMetaData(
		p.Id,
		filePath,
		&gitlab.GetFileMetaDataOptions{Ref: &p.DefaultBranch},
		nil,
	)
	return err == nil
}

func (p Project) CreateHook(webHook WebHook) error {
	hooks, _, err := p.Service.GetGitLabClient().Projects.ListProjectHooks(p.Id, &gitlab.ListProjectHooksOptions{PerPage: 11})
	if err != nil {
		log.Printf("failed to get hooks: %v", err)
		return fmt.Errorf("failed to get hooks: %w", err)
	}
	if len(hooks) > 10 {
		log.Printf("too many hooks(%d) for project %s", len(hooks), p.Name)
		return errors.New(fmt.Sprintf("too many hooks(%d) for project %s", len(hooks), p.Name))
	}

	hook, err := findSingleHook(webHook, hooks)
	if err != nil {
		return err
	}

	checkSsl := true
	if hook == nil {
		_, _, err := p.Service.GetGitLabClient().Projects.AddProjectHook(p.Id, &gitlab.AddProjectHookOptions{
			ConfidentialIssuesEvents: &webHook.ConfidentialIssuesEvents,
			ConfidentialNoteEvents:   &webHook.ConfidentialNoteEvents,
			DeploymentEvents:         &webHook.DeploymentEvents,
			EnableSSLVerification:    &checkSsl,
			IssuesEvents:             &webHook.IssuesEvents,
			JobEvents:                &webHook.JobEvents,
			MergeRequestsEvents:      &webHook.MergeRequestsEvents,
			NoteEvents:               &webHook.NoteEvents,
			PipelineEvents:           &webHook.PipelineEvents,
			PushEvents:               &webHook.PushEvents,
			PushEventsBranchFilter:   &webHook.PushEventsBranchFilter,
			ReleasesEvents:           &webHook.ReleasesEvents,
			TagPushEvents:            &webHook.TagPushEvents,
			Token:                    &webHook.Secret,
			URL:                      &webHook.Url,
			WikiPageEvents:           &webHook.WikiPageEvents,
		})
		if err != nil {
			log.Printf("cannot create webhook for project " + p.Name)
			return fmt.Errorf("cannot create webhook for project %s: %w", p.Name, err)
		}
	} else {
		_, _, err := p.Service.GetGitLabClient().Projects.EditProjectHook(p.Id, hook.ID,
			&gitlab.EditProjectHookOptions{
				ConfidentialIssuesEvents: &webHook.ConfidentialIssuesEvents,
				ConfidentialNoteEvents:   &webHook.ConfidentialNoteEvents,
				DeploymentEvents:         &webHook.DeploymentEvents,
				EnableSSLVerification:    &checkSsl,
				IssuesEvents:             &webHook.IssuesEvents,
				JobEvents:                &webHook.JobEvents,
				MergeRequestsEvents:      &webHook.MergeRequestsEvents,
				NoteEvents:               &webHook.NoteEvents,
				PipelineEvents:           &webHook.PipelineEvents,
				PushEvents:               &webHook.PushEvents,
				PushEventsBranchFilter:   &webHook.PushEventsBranchFilter,
				ReleasesEvents:           &webHook.ReleasesEvents,
				TagPushEvents:            &webHook.TagPushEvents,
				Token:                    &webHook.Secret,
				URL:                      &webHook.Url,
				WikiPageEvents:           &webHook.WikiPageEvents,
			})
		if err != nil {
			log.Printf("cannot edit webhook(#{hook.id}) for project(#{project.Id})")
			return fmt.Errorf("cannot edit webhook(#{hook.id}) for project(#{project.Id}): %w", err)
		}
	}

	return nil
}

func (p Project) CloneGitRepository() (GitRepository, error) {
	filesystem := memfs.New()
	repo, err := git.Clone(memory.NewStorage(), filesystem, &git.CloneOptions{
		Auth: p.Service.GetGitAuth(),
		URL:  p.RepoUrl,
	})
	if err != nil {
		if err.Error() != "remote repository is empty" {
			return GitRepository{}, fmt.Errorf("cannot clone repository %s: %w", p.RepoUrl, err)
		} else {
			repo.Storer.SetReference(plumbing.NewSymbolicReference("HEAD", plumbing.ReferenceName("refs/heads/"+p.DefaultBranch)))
		}
	}

	return GitRepository{
		filesystem: &filesystem,
		repository: repo,
		project:    &p,
	}, nil
}

func (p Project) CreateMergeRequest(options gitlab.CreateMergeRequestOptions) (*gitlab.MergeRequest, error) {
	mr, _, error := p.Service.GetGitLabClient().MergeRequests.CreateMergeRequest(p.Id, &options)
	return mr, error
}

func (p Project) MergeMergeRequest(mr *gitlab.MergeRequest) error {
	trueValue := true
	mergeOptions := gitlab.AcceptMergeRequestOptions{
		Squash:                   &trueValue,
		ShouldRemoveSourceBranch: &trueValue,
	}
	_, response, error := p.Service.GetGitLabClient().MergeRequests.AcceptMergeRequest(p.Id, mr.IID, &mergeOptions)
	if error != nil {
		return error
	}
	if response.StatusCode != 200 {
		return errors.New("could not merge")
	}
	return nil
}

func findSingleHook(webHook WebHook, hooks []*gitlab.ProjectHook) (*gitlab.ProjectHook, error) {
	candidateHooks := make([]*gitlab.ProjectHook, 0, len(hooks))
	for _, h := range hooks {
		if sameWebHookUrl(webHook.Url, h.URL) {
			candidateHooks = append(candidateHooks, h)
		}
	}

	if len(candidateHooks) == 0 {
		return nil, nil
	}

	if len(candidateHooks) > 1 {
		log.Printf("too many hooks matching desired hook")
		return nil, errors.New("too many hooks matching desired hook")
	}

	return candidateHooks[0], nil
}

func sameWebHookUrl(url1 string, url2 string) bool {
	return strings.TrimSuffix(url1, "/") == strings.TrimSuffix(url2, "/")
}
