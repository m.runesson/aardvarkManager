package gitlab

import (
	"fmt"
	"github.com/go-git/go-git/v5/plumbing/transport"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
	gitlab "github.com/xanzy/go-gitlab"
	"log"
)

type gitlabService struct {
	userName     string
	email        string
	token        string
	gitlabClient *gitlab.Client
}

func NewGitLabService(userName string, email string, token string) gitlabService {
	git, err := gitlab.NewClient(token)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	return gitlabService{
		gitlabClient: git,
		userName:     userName,
		email:        email,
		token:        token,
	}
}

func (g gitlabService) GetGitLabClient() *gitlab.Client {
	return g.gitlabClient
}

func (g gitlabService) GetUserInfo() (string, string) {
	return g.userName, g.email
}

func (g gitlabService) GetGitAuth() transport.AuthMethod {
	if g.token == "" {
		return nil
	}
	return &http.BasicAuth{Username: g.userName, Password: g.token}
}

func (g gitlabService) GetProject(repositoryUrl string) (Project, error) {
	project, _, err := g.gitlabClient.Projects.GetProject(repositoryUrl, nil, nil)
	if err != nil {
		log.Printf("Failed to get project: %v", err)
		return Project{}, fmt.Errorf("failed to get project: %w", err)
	}
	return Project{
		Id:            project.ID,
		NamespaceId:   project.Namespace.ID,
		Name:          project.Name,
		Path:          project.PathWithNamespace,
		RepoUrl:       project.HTTPURLToRepo,
		DefaultBranch: project.DefaultBranch,
		Visibility:    project.Visibility,
		Service:       g,
	}, nil
}

func (g gitlabService) CreateProject(options CreateProjectOptions) (Project, error) {
	trueVar := true
	falseVar := false
	createProjectOptions := gitlab.CreateProjectOptions{
		DefaultBranch:                    &options.DefaultBranch,
		Description:                      &options.Description,
		Name:                             &options.Name,
		NamespaceID:                      &options.NamespaceId,
		OnlyAllowMergeIfPipelineSucceeds: &trueVar,
		Path:                             &options.Path,
		RemoveSourceBranchAfterMerge:     &trueVar,
		ContainerRegistryEnabled:         &falseVar,
		IssuesEnabled:                    &trueVar,
		JobsEnabled:                      &falseVar,
		MergeRequestsEnabled:             &trueVar,
		ServiceDeskEnabled:               &falseVar,
		SnippetsEnabled:                  &falseVar,
		Visibility:                       &options.Visibility,
		WikiEnabled:                      &falseVar,
	}
	project, _, err := g.gitlabClient.Projects.CreateProject(&createProjectOptions)
	if err != nil {
		log.Printf("Failed to create project %s: %v", options.Name, err)
		return Project{}, fmt.Errorf("failed to create project %s: %w", options.Name, err)
	}
	return Project{
		Id:            project.ID,
		NamespaceId:   project.Namespace.ID,
		Name:          project.Name,
		Path:          project.PathWithNamespace,
		RepoUrl:       project.HTTPURLToRepo,
		DefaultBranch: project.DefaultBranch,
		Visibility:    project.Visibility,
		Service:       g,
	}, nil
}
