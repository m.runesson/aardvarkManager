package gitlab

import (
	"github.com/go-git/go-billy/v5"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
	"time"
)

type GitRepository struct {
	filesystem *billy.Filesystem
	repository *git.Repository
	project    *Project
}

// CommitAndPush do a commit and push to origin. Return commit hash and error.
func (g GitRepository) CommitAndPush(message string) (string, error) {
	worktree, _ := g.repository.Worktree()
	userName, email := g.project.Service.GetUserInfo()
	g.repository.Config()
	hash, errCommit := worktree.Commit(message, &git.CommitOptions{
		Author: &object.Signature{
			Name:  userName,
			Email: email,
			When:  time.Now(),
		},
	})
	if errCommit != nil {
		return "", errCommit
	}
	errPush := g.repository.Push(&git.PushOptions{Auth: g.project.Service.GetGitAuth()})
	if errPush != nil {
		return "", errPush
	}
	return hash.String(), nil
}

func (g GitRepository) GetFileSystem() *billy.Filesystem {
	return g.filesystem
}

func (g GitRepository) Add(file string) error {
	w, err := g.repository.Worktree()
	if err != nil {
		return err
	}
	_, err = w.Add(file)
	return nil
}

// NewBranch creates a new branch and check it out.
func (g GitRepository) NewBranch(name string) error {
	headRef, _ := g.repository.Head()
	refName := plumbing.ReferenceName("refs/heads/" + name)
	ref := plumbing.NewHashReference(refName, headRef.Hash())
	g.repository.Storer.SetReference(ref)
	branchConfig := &config.Branch{
		Name:   name,
		Remote: "origin",
		Merge:  refName,
		Rebase: "true",
	}

	g.repository.CreateBranch(branchConfig)

	w, _ := g.repository.Worktree()
	w.Checkout(&git.CheckoutOptions{
		Branch: refName,
		Keep:   true,
	})
	return nil
}
