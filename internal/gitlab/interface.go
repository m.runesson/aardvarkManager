package gitlab

import (
	"github.com/go-git/go-billy/v5"
	"github.com/go-git/go-git/v5/plumbing/transport"
	"github.com/xanzy/go-gitlab"
)

type WebHook struct {
	Url                      string
	Secret                   string
	ConfidentialNoteEvents   bool
	PushEvents               bool
	PushEventsBranchFilter   string
	IssuesEvents             bool
	ConfidentialIssuesEvents bool
	MergeRequestsEvents      bool
	TagPushEvents            bool
	NoteEvents               bool
	JobEvents                bool
	PipelineEvents           bool
	WikiPageEvents           bool
	DeploymentEvents         bool
	ReleasesEvents           bool
}

type CreateProjectOptions struct {
	Name          string
	NamespaceId   int
	Path          string
	Description   string
	DefaultBranch string
	Visibility    gitlab.VisibilityValue
}

type GitProjectHost interface {
	GetGitLabClient() *gitlab.Client
	GetUserInfo() (string, string)
	GetGitAuth() transport.AuthMethod
	GetProject(repositoryUrl string) (Project, error)
	CreateProject(options CreateProjectOptions) (Project, error)
}

type GitProject interface {
	CloneGitRepository() (GitRepository, error)
	HasFile(filePath string) bool
	CreateHook(webHook WebHook) error
	GetRepositoryHttpUrl() string
}

type Repository interface {
	CommitAndPush(message string) (string, error)
	GetFileSystem() *billy.Filesystem
	Add(file string) error
}
