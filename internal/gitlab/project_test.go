package gitlab

import (
	"github.com/stretchr/testify/assert"
	"github.com/xanzy/go-gitlab"
	"testing"
)

func TestCloneRepo(t *testing.T) {
	project := Project{
		Id:            0,
		NamespaceId:   0,
		Name:          "",
		Path:          "",
		RepoUrl:       "https://gitlab.com/arbetsformedlingen/devops/aardvark.git",
		DefaultBranch: "",
		Service:       NewGitLabService("testuser", "testemail", ""),
	}
	repo, err := project.CloneGitRepository()
	assert.Nil(t, err)
	assert.NotNil(t, repo)
}

func TestFindSingleHookFindsASingleHook(t *testing.T) {
	matchingHook := gitlab.ProjectHook{
		URL: "https://my.hook",
	}
	input := []*gitlab.ProjectHook{
		{
			URL: "https://not.my.hook",
		},
		&matchingHook,
	}
	result, error := findSingleHook(WebHook{Url: "https://my.hook"}, input)
	assert.Nil(t, error)
	assert.Equal(t, &matchingHook, result)
}

func TestFindSingleHookFindsMultipleMatch(t *testing.T) {
	matchingHook := gitlab.ProjectHook{
		URL: "https://my.hook",
	}
	input := []*gitlab.ProjectHook{
		&matchingHook,
		{
			URL: "https://not.my.hook",
		},
		&matchingHook,
	}
	result, error := findSingleHook(WebHook{Url: "https://my.hook"}, input)
	assert.NotNil(t, error)
	assert.Nil(t, result)
}

func TestFindSingleHookWithNoMatch(t *testing.T) {
	input := []*gitlab.ProjectHook{
		{
			URL: "https://not.my.hook",
		},
	}
	result, error := findSingleHook(WebHook{Url: "https://my.hook"}, input)
	assert.Nil(t, error)
	assert.Nil(t, result)
}

func TestSameWebHookUrlWhenArgumentsAreEqual(t *testing.T) {
	assert.True(t, sameWebHookUrl("https://foo.com/bar", "https://foo.com/bar"))
}

func TestSameWebHookUrlWhenArgumentsAreEqualWithExtraSlash(t *testing.T) {
	assert.True(t, sameWebHookUrl("https://foo.com/bar/", "https://foo.com/bar"))
}

func TestSameWebHookUrlWhenArgumentsAreNotEqual(t *testing.T) {
	assert.False(t, sameWebHookUrl("https://foo.com/bar", "https://foo.com/bar1"))
}
