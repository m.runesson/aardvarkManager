package configGenerator

import (
	"bufio"
	"github.com/go-git/go-billy/v5"
	"github.com/go-git/go-billy/v5/memfs"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"os"
	"testing"
)

var fixture struct {
	fs         *billy.Filesystem
	addedFiles []string
}

type GitMock struct {
	mock.Mock
}

func (g GitMock) GetFileSystem() *billy.Filesystem {
	return fixture.fs
}

func (g GitMock) Add(file string) error {
	fixture.addedFiles = append(fixture.addedFiles, file)
	return nil
}

func setup() {
	fs := memfs.New()
	fixture.fs = &fs
}

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	os.Exit(code)
}

func TestCreateKustomize(t *testing.T) {
	repoMock := new(GitMock)
	CreateKustomize(repoMock, "sample", []string{"test"})
	assert.Equal(t,
		[]string{"kustomize/base/deployment.yaml",
			"kustomize/base/hpa.yaml",
			"kustomize/base/kustomization.yaml",
			"kustomize/base/route.yaml",
			"kustomize/base/service.yaml",
			"kustomize/base/serviceaccount.yaml",
			"kustomize/overlays/test/kustomization.yaml"},
		fixture.addedFiles)
	result := readFile(*fixture.fs, "kustomize/overlays/test/kustomization.yaml")
	expected := `apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
bases:
  - ../../base
namespace: sample-test
images:
  - name: docker-images.jobtechdev.se/sample/sample:latest
    newName: docker-images.jobtechdev.se/sample/sample
    newTag: "0000000"
`
	assert.Equal(t, expected, result)
}

func readFile(fs billy.Filesystem, filename string) string {
	f, _ := fs.Open(filename)
	defer f.Close()
	result := ""
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		result = result + scanner.Text() + "\n"
	}
	return result
}
