package configGenerator

import (
	"embed"
	"github.com/go-git/go-billy/v5"
	"text/template"
)

//go:embed infra-templates
var tmplFS embed.FS

type simpleGitRepository interface {
	GetFileSystem() *billy.Filesystem
	Add(file string) error
}

func renderTemplateToFile(templates *template.Template, templateName string, destinationFS billy.Filesystem, destinationFileName string, data any) error {
	file, err := destinationFS.Create(destinationFileName)
	if err != nil {
		return err
	}
	return templates.ExecuteTemplate(file, templateName, data)
}
