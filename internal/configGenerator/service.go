package configGenerator

import (
	"fmt"
	gitlab2 "github.com/xanzy/go-gitlab"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/config"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/internal/gitlab"
	"log"
	"time"
)

type serviceConfig struct {
	environments  map[string]config.Environment
	argocdProject gitlab.Project
}

type gitlabService interface {
	GetProject(repositoryUrl string) (gitlab.Project, error)
}

func CreateService(environments map[string]config.Environment, gitlabService gitlabService, argocdProjectPath string) *serviceConfig {
	argocdProject, err := gitlabService.GetProject(argocdProjectPath)
	if err != nil {
		log.Fatalln("Unknown ArgoCD repository "+argocdProjectPath, err)
	}
	return &serviceConfig{
		environments:  environments,
		argocdProject: argocdProject,
	}
}

func (s *serviceConfig) AddArgoCDApplications(projectName string, environments []string, ownerTeam string, infraProjectGitUrl string) (string, error) {
	if len(environments) == 1 && environments[0] == "develop" {
		return "", nil
	}

	repository, err := s.argocdProject.CloneGitRepository()
	if err != nil {
		return "", err
	}

	branchName := fmt.Sprintf("am-%s-%d", projectName, time.Now().UnixNano())
	err = repository.NewBranch(branchName)
	if err != nil {
		return "", err
	}

	err = s.createArgoCDApplicationFiles(repository, projectName, environments, ownerTeam, infraProjectGitUrl)
	if err != nil {
		return "", err
	}

	commitMsg := "Adding project " + projectName
	_, err = repository.CommitAndPush(commitMsg)
	if err != nil {
		return "", err
	}

	title := "Adding applications for " + projectName
	removeSrcBranch := true
	squash := false
	collaboration := false
	mr, err := s.argocdProject.CreateMergeRequest(gitlab2.CreateMergeRequestOptions{
		Title:              &title,
		Description:        &commitMsg,
		SourceBranch:       &branchName,
		TargetBranch:       &s.argocdProject.DefaultBranch,
		RemoveSourceBranch: &removeSrcBranch,
		Squash:             &squash,
		AllowCollaboration: &collaboration,
	})
	if err != nil {
		return mr.WebURL, err
	}
	return mr.WebURL, nil
}
