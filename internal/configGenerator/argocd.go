package configGenerator

import (
	"github.com/go-git/go-billy/v5"
	"gitlab.com/arbetsformedlingen/devops/aardvarkManager/internal/memfsutil"
	"gopkg.in/yaml.v3"
	"sigs.k8s.io/kustomize/api/types"
	"strings"
	"text/template"
)

const argocdTemplatePath = "infra-templates/argocd/application.yaml"
const kustomizationFileName = "kustomization.yaml"

var applicationTemplate, _ = template.ParseFS(tmplFS, argocdTemplatePath)

type applicationTemplateValues struct {
	ProjectName        string
	Environment        string
	Team               string
	InfraProjectGitUrl string
}

func (s *serviceConfig) createArgoCDApplicationFiles(repo simpleGitRepository, projectName string, environments []string, ownerTeam string, infraProjectGitUrl string) error {
	lowerProjectName := strings.ToLower(projectName)
	destinationFS := repo.GetFileSystem()
	rootPath := "kustomize/clusters/"
	for _, e := range environments {
		dirname := rootPath + s.environments[e].Cluster + "/"
		filename := lowerProjectName + "-" + e + ".yaml"
		if e != "develop" && !memfsutil.HasFile(destinationFS, dirname+filename) {
			tmplValues := applicationTemplateValues{
				ProjectName:        lowerProjectName,
				Environment:        e,
				Team:               ownerTeam,
				InfraProjectGitUrl: infraProjectGitUrl,
			}

			err := renderTemplateToFile(applicationTemplate,
				"application.yaml",
				*destinationFS,
				dirname+filename,
				tmplValues)
			if err != nil {
				return err
			}
			err = repo.Add(dirname + filename)
			if err != nil {
				return err
			}
			// Long term this shall be changed so only one edit of each kustomization.yaml happens.
			err = s.addManifestsToKustomize(destinationFS, dirname, filename)
			if err != nil {
				return err
			}
			err = repo.Add(dirname + kustomizationFileName)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (s *serviceConfig) addManifestsToKustomize(fs *billy.Filesystem, directory string, manifestFileName string) error {
	kustomizationFilePath := directory + kustomizationFileName
	kustomizationData, err := memfsutil.ReadFile(*fs, kustomizationFilePath)
	if err != nil {
		return err
	}
	kustomize, err := unmarshalToKustomization([]byte(kustomizationData))
	kustomize.Resources = append(kustomize.Resources, manifestFileName)
	outData, err := MarshalKustomization(kustomize)
	if err != nil {
		return err
	}
	return memfsutil.WriteFile(*fs, kustomizationFilePath, outData)
}

func unmarshalToKustomization(data []byte) (*types.Kustomization, error) {
	kustomization := types.Kustomization{}
	err := yaml.Unmarshal(data, &kustomization)
	if err != nil {
		return nil, err
	}
	return &kustomization, nil
}

func MarshalKustomization(kustomization *types.Kustomization) ([]byte, error) {
	return yaml.Marshal(kustomization)
}
