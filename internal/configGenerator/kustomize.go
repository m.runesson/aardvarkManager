package configGenerator

import (
	"io/fs"
	"strings"
	"text/template"
)

var baseDirTemplates, _ = template.ParseFS(tmplFS, "infra-templates/base/*")
var overlayDirTemplates, _ = template.ParseFS(tmplFS, "infra-templates/env-overlay/*")

type kustomizeTemplateValues struct {
	ProjectName string
	Environment string
}

func CreateKustomize(repo simpleGitRepository, projectName string, environments []string) error {
	lowerProjectName := strings.ToLower(projectName)
	err := createBaseLayer(repo, lowerProjectName)
	if err != nil {
		return err
	}

	return createOverlays(repo, environments, lowerProjectName)
}

func createBaseLayer(repo simpleGitRepository, projectName string) error {
	destinationFS := repo.GetFileSystem()
	rootPath := "infra-templates/base"
	tmplValues := kustomizeTemplateValues{ProjectName: projectName, Environment: "NOT-USED-IN-BASE"}
	return fs.WalkDir(tmplFS, rootPath, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		if !d.IsDir() {
			rootRelPath := strings.TrimPrefix(path, rootPath)
			filename := "kustomize/base" + strings.ToLower(rootRelPath)
			err = renderTemplateToFile(baseDirTemplates,
				d.Name(),
				*destinationFS,
				filename,
				tmplValues)
			if err != nil {
				return err
			}
			return repo.Add(filename)
		}
		return nil
	})
}

func createOverlays(repo simpleGitRepository, environments []string, projectName string) error {
	// Render overlay manifest for each environment
	destinationFS := repo.GetFileSystem()
	rootPath := "infra-templates/env-overlay"
	for _, env := range environments {
		lowerEnv := strings.ToLower(env)
		tmplEnvValues := kustomizeTemplateValues{ProjectName: projectName, Environment: lowerEnv}
		err := fs.WalkDir(tmplFS, rootPath, func(path string, d fs.DirEntry, err error) error {
			if err != nil {
				return err
			}
			if !d.IsDir() {
				rootRelPath := strings.TrimPrefix(path, rootPath)
				filename := "kustomize/overlays/" + lowerEnv + strings.ToLower(rootRelPath)
				err = renderTemplateToFile(overlayDirTemplates,
					d.Name(),
					*destinationFS,
					filename,
					tmplEnvValues)
				if err != nil {
					return err
				}
				return repo.Add(filename)
			}
			return nil
		})
		if err != nil {
			return err
		}
	}
	return nil
}
