package stringutil

func IsMemberOf(elem string, elemList []string) bool {
	for _, e := range elemList {
		if e == elem {
			return true
		}
	}
	return false
}

func HasOnlyUniqueStrings(elemList []string) bool {
	set := make(map[string]bool)
	for _, e := range elemList {
		set[e] = true
	}
	return len(set) == len(elemList)
}
