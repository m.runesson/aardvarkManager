package stringutil

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestIsMemberOfWithMatch(t *testing.T) {
	assert.True(t, IsMemberOf("foo", []string{"bar", "foo", "gazonk"}))
}

func TestIsMemberOfWithNoMatch(t *testing.T) {
	assert.False(t, IsMemberOf("foo", []string{"bar", "gazonk"}))
}

func TestIsMemberOfWithEmptyList(t *testing.T) {
	assert.False(t, IsMemberOf("foo", []string{}))
}

func TestHasOnlyUniqueStrings(t *testing.T) {
	assert.True(t, HasOnlyUniqueStrings([]string{"bar", "foo", "gazonk"}))
}

func TestHasOnlyUniqueStringsWithDuplicate(t *testing.T) {
	assert.False(t, HasOnlyUniqueStrings([]string{"bar", "foo", "gazonk", "foo"}))
}
