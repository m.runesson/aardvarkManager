package memfsutil

import (
	"bufio"
	"github.com/go-git/go-billy/v5"
)

func ReadFile(fs billy.Filesystem, filePath string) (string, error) {
	file, errFile := fs.Open(filePath)

	defer file.Close()
	if errFile != nil {
		return "", errFile
	}
	scanner := bufio.NewScanner(file)
	data := ""
	for scanner.Scan() {
		data += scanner.Text() + "\n"
	}
	return data, nil
}

func WriteFile(fs billy.Filesystem, filePath string, data []byte) error {
	fileWrite, errFileWrite := fs.Create(filePath)
	defer fileWrite.Close()
	if errFileWrite != nil {
		return errFileWrite
	}
	_, err := fileWrite.Write(data)
	return err
}

func HasFile(fs *billy.Filesystem, filename string) bool {
	_, err := (*fs).Stat(filename)
	return err == nil
}
