package kubernetesService

import (
	"context"
	argocd "github.com/argoproj/argo-cd/v2/pkg/client/clientset/versioned/typed/application/v1alpha1"
	userv1 "github.com/openshift/client-go/user/clientset/versioned/typed/user/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"log"
)

type serviceConfig struct {
	clientset    *kubernetes.Clientset
	userClient   *userv1.UserV1Client
	argocdClient *argocd.ArgoprojV1alpha1Client
}

func CreateService() *serviceConfig {
	config, err := rest.InClusterConfig()
	if err != nil {
		kubeConfigPath := "/home/magru/.kube/config"
		config, err = clientcmd.NewNonInteractiveDeferredLoadingClientConfig(
			&clientcmd.ClientConfigLoadingRules{ExplicitPath: kubeConfigPath},
			&clientcmd.ConfigOverrides{}).ClientConfig()
		if err != nil {
			log.Fatalln("Cannot configure kubernetes client", err)
		}
		log.Println("KubernetesService client configured for out of cluster")
	} else {
		log.Println("KubernetesService client configured for in cluster")
	}
	clientSet, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.Fatalln("Problem creating KubernetesService clients", err)
	}
	userClient, err := userv1.NewForConfig(config)
	if err != nil {
		log.Fatalln("Problem creating OpenShift user client", err)
	}
	argocdClient, err := argocd.NewForConfig(config)
	if err != nil {
		log.Fatalln("Problem creating ArgoCD client", err)
	}
	return &serviceConfig{
		argocdClient: argocdClient,
		userClient:   userClient,
		clientset:    clientSet,
	}
}

func (s *serviceConfig) IsExistingGroup(groupName string) bool {
	_, err := s.userClient.Groups().Get(context.Background(), groupName, v1.GetOptions{})
	return err == nil
}

func (s *serviceConfig) GetAllGroups() []string {
	groups, err := s.userClient.Groups().List(context.Background(), v1.ListOptions{})
	if err != nil {
		return []string{}
	}
	var result []string
	for _, g := range groups.Items {
		result = append(result, g.Name)
	}
	return result
}
