package kubernetesService

import (
	"context"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (s *serviceConfig) GetAllAppProjectNames(namespace string) []string {
	appProjects, err := s.argocdClient.AppProjects(namespace).List(context.Background(), v1.ListOptions{})
	if err != nil {
		return []string{}
	}
	var result []string
	for _, p := range appProjects.Items {
		result = append(result, p.Name)
	}
	return result
}
