# TODO

Next: 
- Fix auto merge in ns-pipeline
- Fix review pipeline for argocd-infra


### Kustomize templates

- [ ] ConfigMapGenerator example 
- [ ] Labels

## Later

- [ ] Support for cron
- [ ] Support for persistence
- [ ] Support for secrets
- [ ] If creating webhook for src repo, trigger build of HEAD.
