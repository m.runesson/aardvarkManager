FROM docker.io/library/golang:1.18.3 AS build

WORKDIR /app

COPY . .
RUN go mod download && go mod verify
RUN mkdir bin
# Get bin static linked
ENV CGO_ENABLED=0
RUN go test ./...
RUN go build -v -o bin ./...


FROM scratch

WORKDIR /
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /app/bin/aardvarkManager /aardvarkManager

EXPOSE 8080

ENTRYPOINT ["/aardvarkManager"]