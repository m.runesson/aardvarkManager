# Aardvark Manager

Aardvark Manager is a service helping teams to get started with 
[Aardvark](https://gitlab.com/arbetsformedlingen/devops/aardvark).

The user calls Aardvark Manager with a GitLab path to the
app-repository with  a Dockerfile. The manager ensures the
repository get a webhook to call Aardvark and creates an 
infrastructure repository with initial manifests for 
deployment of the app.

## 

Aardvark Manager is expected to run behind an authenticating proxy. The proxy
shall set two header `HTTP_X_FORWARDED_EMAIL` and `HTTP_X_FORWARDED_GROUPS`. First
one contains the loggedin user's username and second a list of groups the user is
a member of. The group list is comma separated without any spaces.

## API

### project endpoint

POST a JSON to the endpoint containing one field `project`.
Response is a JSON object with metadata about the source and
infrastructure project.

#### Example

```shell
curl -X POST http://localhost:8080/api/v1/project -i \
-d '{"project": "arbetsformedlingen/devops/runmmlab/test", "owner_team": "calamari", "environments": ["develop", "test"]}' \
-H "Accept: application/json" -H "Content-Type: application/json"  \
-H "X-Forwarded-Groups: calamari" -H "X-Forwarded-Email: myuser"
```

Response will look like:

```json
{
  "src_project": {
    "project_id": 37456946,
    "namespace_id": 13607912,
    "name": "test",
    "path": "arbetsformedlingen/devops/runmmlab/test",
    "http_repo_url": "https://gitlab.com/arbetsformedlingen/devops/runmmlab/test.git",
    "default_branch": "main"
  },
  "infra_project": {
    "project_id": 37618220,
    "namespace_id": 13607912,
    "name": "test infra",
    "path": "arbetsformedlingen/devops/runmmlab/test-infra",
    "http_repo_url": "https://gitlab.com/arbetsformedlingen/devops/runmmlab/test-infra.git",
    "default_branch": "main"
  },
  "merge_request_namespaces":"https://gitlab.com/arbetsformedlingen/devops/ns-conf-infra/-/merge_requests/26",
  "merge_request_argocd":"https://gitlab.com/arbetsformedlingen/devops/argocd-infra/-/merge_requests/42"
}
```

At the moment you must merge the merge request returned in `merge_request_namespaces` and `merge_request_argocd`.
